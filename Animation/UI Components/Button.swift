//
//  Button.swift
//  Animation
//
//  Created by Edward Chandra on 05/01/20.
//  Copyright © 2020 Edward Chandra. All rights reserved.
//

import UIKit

class Button: UIButton{
    override init(frame: CGRect) {
        super.init(frame:frame)
        self.setupStyle()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder:coder)
        self.setupStyle()
    }
    
    private func setupStyle(){
        self.layer.cornerRadius = 10
        self.layer.backgroundColor = UIColor.white.cgColor
        self.setTitleColor(.systemBlue, for: .normal)
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowRadius = 2
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowOpacity = 0.2
    }
}

extension Button{
    func testClick(){
        print("testClick")
    }
}

