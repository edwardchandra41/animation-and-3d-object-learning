//
//  ViewController.swift
//  Animation
//
//  Created by Edward Chandra on 05/01/20.
//  Copyright © 2020 Edward Chandra. All rights reserved.
//

import UIKit
import SpriteKit

class ViewController: UIViewController {

    @IBOutlet weak var basicButton: Button!
    @IBOutlet weak var particleButton: Button!
    
    @IBOutlet weak var backgroundView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }

    @IBAction func basicButtonAction(_ sender: Button) {
        sender.testClick()
        
        sender.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        
        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 5.0, options: .curveEaseIn, animations: {
            sender.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    @IBAction func particleButtonAction(_ sender: Button) {
        sender.testClick()
        
        let sk: SKView = SKView()
        sk.frame = backgroundView.bounds
        sk.backgroundColor = .clear
        backgroundView.addSubview(sk)
        
        let scene: SKScene = SKScene(size: backgroundView.bounds.size)
        scene.scaleMode = .aspectFit
        scene.backgroundColor = .clear
        
        let en = SKEmitterNode(fileNamed: "SnowParticle.sks")
        en?.position = CGPoint(x: backgroundView.frame.width/2, y: backgroundView.frame.height)
        
        scene.addChild(en!)
        sk.presentScene(scene)
    }
}

